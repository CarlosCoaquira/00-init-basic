-- curso de git
gestos de paquetes: aptitude

comandos UNIX:
	whatis
	ps -ef
	pstree -Agu
	kill -9 #PID
	killall firefox
	jobs
	top
	sort fichero
	sort -r fichero
	tail fichero
	tail -n 3 fichero
	head -n 6 fichero
	wc fichero
	cut -c 3-9 fichero
	cut -d ":" -f1 fichero
	ls -lh / | rev | cut -d ":" -f1 | rev | sort
	grep terminobuscado fichero
	grep ^terminobuscado fichero
	grep terminobuscado$ fichero
	grep -n error ruta/fichero
	grep -c error ruta/fichero
	ls -lh | grep termino
	find /ruta -name "*.extension"
	find /ruta -size +1024k -size -2024k
	find / -iname "nombr*"
	find / -name "nombre*"

comandos GIT:
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry
git init
-------------------------------------------------------
-- se tiene que estar fuera de la carpeta del pry
git clone ./01-hola-mundo/ 01-hola-mundo-CLON
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry
git status
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- se especifica el archivo/ruta que se desea añadir que aparece con leyenda "Untracked files:"
git add src/app/components/
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- se añade todo automaticamente
git add
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- se retira (no elimina) archivo de la lista de cambios a comitear
git rm --cached archivo
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- revierte un cambio (rollback) de fichero desde la imagen de GIT
git checkout src/index.html
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- compara version actual y version de GIT
git diff src/index.html
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- crear archivo ".gitignore" para añadir archivos que no se van a comitear
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- commitear
git commit -m "Version inicial del primer ejercicio del curso Angular"
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- ver commits ejecutados
git log --oneline
-- ver log completo
git log
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- commitear sin hacer "git add ."
git commit -am "Version modificada el 20200718"
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- comparar dos commits. Prevaviamente se debe ejecutar git log y obtener los ides
git diff fd905d9 bbf19e7
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- luego de probar, para revertir el cambio primero se debe ejecutar el reset y luego el checkout
-- retira un cambio (rollback)  de fichero  del indice desde la imagen de GIT
git reset HEAD src/index.html
-- retira todos los cambios  del indice desde la imagen de GIT
git reset .
-- revierte cambios desde la imagen de GIT
git checkout src/index.html
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- revertir o deshacer commits. Esto tambien deshace el codigo en los archivos
-- ejecutar primero git log. Los resultados se leen de arriba hacia abajo, empezo desde cero (indice).
git log
-- asignar el indice que sera el actual al final del comando. Esto borra los posteriores commit
git reset --hard HEAD~0
-- otro modo de hacerlo, es ejecutando el
git log --oneline
-- precisar el codigo alfanumerico del commit que se establecera como actual al final del comando. Esto borra el commit posterior al deseado.
git reset --hard bbf19e7
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- revertir o deshacer commits SIN deshacer el codigo en los archivos
git reset HEAD~1
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- saltar entre commits anteriores o posteriores. 
-- Ejecutar antes:
git log --oneline
-- Se coloca el indice del commit deseado al final del comenado. commit anterior. Deshace commit y cambios en archivos.
git checkout bbf19e7
-- Se coloca el indice del commit deseado al final del comenado. commit posterior. Reconstruye commit  y cambios en archivos.
git checkout 88104af
-- para confirmar cambios despues del checkout. 
-- Sin embargo, si se hizo un commit adicional mientras se estaba haciendo los saltos, este commit se perdera y 
-- para evitar ello, se debe generar una rama. . Afecta a archivos
git checkout master
-- para crear rama con commit posterior de 88104af
git branch rama_20200728  e25eabb
-- para ver ramas
git branch
-- para ver commit de la rama nueva (cambia de rana). Afecta a archivos
git checkout rama_20200728
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- buscar commit por comentario
git log --grep=inicial
-- buscar commit por contenido
git log -S "v2"
-- con el resultado es posible hacer un checkout al commit deseado
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- trabajo con ramas. Cada rama creada hereda los commit del MASTER
-- para fines del ejercicio se creo:
git branch certificar
git branch desarrollo
git branch homologar
git branch contacto
-- cambiando de rama
git checkout contacto
-- para eliminar rama
git branch -D certificar
-- en cada rama se trabaja funcionalidades separadas y se aplica MERGE con rama master
-- haciendo merge de master con contacto. Se lee desde contacto hasta master
-- se debe estar situado en Master y se consolidan los commit en MASTER
git merge contacto
-- de igual se puede hacer merge desde el master a otras ramas
git checkout desarrollo
git merge master
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- cambiar de rama sin llevar mis cambios actuales a otra rama y sin perder los cambios en la rama actual (sin commit)
-- reserva temporalmente los cambios que luego se deben recuperar. se debe estar en la rama de trabajo con cambios. luego se puede cambiar de rama
git stash
-- para ver lo que guardo
git stash show
-- recuperar cambios grabados temporalmente
git stash pop
-- liberar cambios grabados temporalmente
git stash clear
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- replicar solo un commit en particular (el ultimo) desde una rama X a master
-- ubicasrse en master
git checkout master
-- usar el comando indicando al final el identificar del commit de la rama X a combinar/añadir en Master
git cherry-pick 79494d9
-- para abortar en caso de error
git cherry-pick --abort
-------------------------------------------------------
-- se tiene que estar dentro de la carpeta del pry.
-- conflictos, se coorige a mano luego que GIT marca los conflictos en el archivo con el codigo fuente
-- luego se hace commit del archivo final
-------------------------------------------------------
-- subiendo a Gitlab.
-- crear proyecto en web gitlab
-- generar archivo readme / este se puede editar para añadir mas informacion de interes
touch README.md
-- comitear archivo readme
-- añadir/vincular repo remoto
git remote add origin git@gitlab.com:CarlosCoaquira/01-hola-mundo.git
-- subir proyecto nuevo entero a gitlab
git push -u origin master
-- para descargar desde Gitlab cambios de una RAMA NUEVA. como ejemplo se creo una rama nueva "usuario" en gitlab y 
-- se creo la misma rama en local y luego actualizo desde el remoto
git branch usuario
git fetch --all
-- para descargar desde Gitlab cambios de una RAMA EXISTENTE
git pull origin master
-- para subir cambios/commit nuevos de proyecto existente
git push origin master

-------------------------------------------------------

-------------------------------------------------------